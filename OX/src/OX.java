/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
import java.util.Scanner;

public class OX {

    static void printBoard(char[][] board) {
        System.out.println("  1 2 3");
        for (int i = 0; i < board.length; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    static void startGame(char[][] board) {
        System.out.println("+--------------------+");
        System.out.println("| Welcome to OX Game |");
        System.out.println("+--------------------+");
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = '-';
            }
        }
    }

    static void inputO(char[][] board, int r, int c) {
        if (board[r - 1][c - 1] == '-') {
            board[r - 1][c - 1] = 'O';
        } else {
            System.out.println("----- used -------");
        }

    }

    static void inputX(char[][] board, int r, int c) {
        if (board[r - 1][c - 1] == '-') {
            board[r - 1][c - 1] = 'X';
        } else {
            System.out.println("----- used -------");
        }

    }

    static boolean checkDraw(char[][] board, int count, boolean finish) {
        if (count == 9) {
            finish = true;
            System.out.println("----- Draw!!! -----");
        }
        return finish;
    }

    static boolean checkWin(char[][] board, int count, boolean finish) {
        printBoard(board);
        int countp1 = 0;
        int countp2 = 0;
        if (count >= 4) {
            for (int i = 0; i < 3; i++) {
                int count_row = 0;
                int count_col = 0;
                if ((board[0][0] == board[i][i]) && (board[0][0] != '-')) {
                    countp1 += 1;
                    if (countp1 == 3) {
                        finish = true;
                        System.out.println("----- " + board[0][0] + " Win -----");
                    }
                }
                if ((board[0][2] == board[i][2 - i]) && (board[0][2] != '-')) {
                    countp2 += 1;
                    if (countp2 == 3) {
                        finish = true;
                        System.out.println("----- " + board[0][2] + " Win -----");
                    }
                }
                for (int j = 2; j > 0; j--) {
                    if ((board[i][0] == board[i][j]) && (board[i][0] != '-')) {
                        count_col += 1;
                        if (count_col == 2) {
                            finish = true;
                            System.out.println("----- " + board[i][0] + " Win -----");
                        }
                    }
                    if ((board[0][i] == board[j][i]) && (board[0][i] != '-')) {
                        count_row += 1;
                        if (count_row == 2) {
                            finish = true;
                            System.out.println("----- " + board[0][i] + " Win -----");
                        }
                    }
                }
            }
        }
        finish = checkDraw(board, count, finish);
        return finish;
    }

    static void turnGame(char[][] board) {
        Scanner kb = new Scanner(System.in);
        boolean finish = false;
        int count = 0;
        do {
            count++;
            turnO(board);
            finish = checkWin(board, count, finish);

            if (finish == true || count == 9) {
                break;
            }

            count++;
            turnX(board);
            finish = checkWin(board, count, finish);

        } while(!finish);
    }

    static void turnO(char[][] board) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Turn O");
        System.out.print("Please input row, col : ");
        inputO(board, kb.nextInt(), kb.nextInt());
    }

    static void turnX(char[][] board) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Turn X");
        System.out.print("Please input row, col : ");
        inputX(board, kb.nextInt(), kb.nextInt());
    }

    static void thankyou() {
        System.out.println("+---------------------------+");
        System.out.println("| Thank You To Play OX Game |");
        System.out.println("+---------------------------+");
    }

    public static void main(String[] args) {
        char[][] board = new char[3][3];
        startGame(board);
        printBoard(board);
        turnGame(board);
        thankyou();
    }

}
