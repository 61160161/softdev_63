
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author informatics
 */
class Game {

    Scanner kb = new Scanner(System.in);
    private Table board;
    private Player player;
    Player o;
    Player x;
    char whoWin;
    
    Game(){
        o = new Player('O');
        x = new Player('X');
    }
    
    void printWelcome() {
        System.out.println("+--------------------+");
        System.out.println("| Welcome to OX Game |");
        System.out.println("+--------------------+");
    }

    void turnGame() {
        board = new Table();
        Scanner kb = new Scanner(System.in);
        boolean finish = false;
        int count = 0;
        do {
            count++;
            this.turnO();
            finish = board.checkWin(count, finish);

            if (finish == true || count == 9) {
                break;
            }

            count++;
            this.turnX();
            finish = board.checkWin(count, finish);

        } while (!finish);
    }

    void turnO() {
        System.out.println("Turn O");
        System.out.print("Please input row, col : ");
        board.inputO(kb.nextInt(), kb.nextInt());
    }

    void turnX() {
        System.out.println("Turn X");
        System.out.print("Please input row, col : ");
        board.inputX(kb.nextInt(), kb.nextInt());
    }

    char goContinue() {
        char input;
        do {
            System.out.print("Continue ? (y/n) : ");
            input = kb.next().charAt(0);

            if (input == 'n' || input == 'y') {
                break;
            }

        } while (true);

        return input;
    }
    
    void addScore(){
        whoWin = board.printWin();
    if (whoWin == 'O') {
            o.setWin();
            x.setLost();
        } else if (whoWin == 'X') {
           x.setWin();
            o.setLost();
        } else if (whoWin == 'd') {
            x.setDraw();
            o.setDraw();
        }
    }
    
    static void thankyou() {
        System.out.println("+---------------------------+");
        System.out.println("| Thank You To Play OX Game |");
        System.out.println("+---------------------------+");
    }


    void run() {
        board = new Table();
        this.printWelcome();
        char play;
        do {
            board.setGame();
            board.printBoard();
            this.turnGame();
            this.addScore();
            o.printScore();
            x.printScore();
            play = this.goContinue();
        } while (play == 'y');
        this.thankyou();
    }

}
