
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String img;

    public Product(int id, String name, double price, String img) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", img=" + img + '}';
    }
    
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList();
        list.add(new Product(1,"Espresso1",40,"1.png"));
        list.add(new Product(2,"Espresso2",30,"1.png"));
        list.add(new Product(3,"Espresso3",40,"1.png"));
        list.add(new Product(4,"Americano1",30,"2.png"));
        list.add(new Product(5,"Americano2",40,"2.png"));
        list.add(new Product(6,"Americano3",50,"2.png"));
        list.add(new Product(7,"ChaYen1",40,"3.png"));
        list.add(new Product(8,"ChaYen2",40,"3.png"));
        list.add(new Product(9,"ChaYen3",40,"3.png"));
        return list;
    }
}
